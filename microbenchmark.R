
if(!require("microbenchmark")) {
  install.packages("microbenchmark")
}
library("microbenchmark")
library("ggplot2")

forloop = function(x) {
  mean = 0
  for(i in 1:length(x)) {
    mean = mean + x[i]
  }
  return(mean/length(x))
}

forloop(x[,1])

value = 1000
x = matrix(replicate(10, rnorm(value)), 10, nrow = value, ncol = 10)
View(x)

mbm = microbenchmark(
  'meanfun' = mean(x[,1]),
  'forloop' = forloop(x[,1])
)

autoplot(mbm)

